﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RouletteManager.Core.DTOs
{
    public class UsersDTO
    {   
        public int IdUser { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public decimal Credit { get; set; }
        public int State { get; set; }
        public String token { get; set; }

    }
}
