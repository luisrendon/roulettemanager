﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RouletteManager.Core.DTOs
{
    public class RouletteDTO
    {
        public int IdRoulette { get; set; }
        public int State { get; set; }
    }
}
