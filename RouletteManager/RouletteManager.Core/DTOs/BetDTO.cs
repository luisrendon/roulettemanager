﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RouletteManager.Core.DTOs
{
    public class BetDTO
    {
        public int IdBet { get; set; }
        public int IdRouletteResult { get; set; }
        public decimal BetNumber { get; set; }
        public decimal BetValue { get; set; }
        public decimal BetColor { get; set; }
        public int State { get; set; }
        public int IdRoulette { get; set; }
        public String ResponseMesagge { get; set; }
        public String ResponseCode { get; set; }

    }
}
