﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RouletteManager.Core.DTOs
{
    public class RouletteResultDTO
    {
        public int IdRouletteResult { get; set; }
        public int IdRoulette { get; set; }
        public decimal Acumulated { get; set; }
        public decimal? ColorResult { get; set; }
        public decimal? NumerResult { get; set; }
        public String ResponseMesagge { get; set; }
        public String ResponseCode { get;set; }
    }
}
