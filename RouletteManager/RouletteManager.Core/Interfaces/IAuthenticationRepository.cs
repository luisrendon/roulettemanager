﻿using RouletteManager.Core.Data;
using System.Threading.Tasks;

namespace RouletteManager.Core.Interfaces
{
    public interface IAuthenticationRepository
    {
        bool Authentication(Users users);

    }
}
