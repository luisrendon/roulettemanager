﻿using RouletteManager.Core.Data;
using System.Threading.Tasks;

namespace RouletteManager.Core.Interfaces
{
    public interface IBetRepository
    {
        Task<Bet> postBet(Bet bet);
    }
}
