﻿using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using System.Threading.Tasks;

namespace RouletteManager.Core.Services
{
    public interface IRouletteResultService
    {
        Task<RouletteResultDTO> postRouletteResult(RouletteResultDTO rouletteResultDTO);
    }
}
