﻿using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouletteManager.Core.Services
{
    public interface IRouletteService
    {
        Task<IEnumerable<RouletteDTO>> GetRoulettes();
        Task<RouletteDTO> GetRoulette(int id);
        Task<RouletteDTO> PostRoulette(RouletteDTO roulette);
    }
}
