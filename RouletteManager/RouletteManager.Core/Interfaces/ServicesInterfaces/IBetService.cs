﻿using RouletteManager.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RouletteManager.Core.Services
{
    public interface IBetService
    {
        Task<BetDTO> PostBet(BetDTO betDTO, int idUser);
    }
}
