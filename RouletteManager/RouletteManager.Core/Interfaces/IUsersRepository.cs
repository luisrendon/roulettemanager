﻿using RouletteManager.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RouletteManager.Core.Interfaces
{
    public interface IUsersRepository
    {
        Task<Users> getUserById(int idUser);
    }
}
