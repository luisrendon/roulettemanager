﻿using RouletteManager.Core.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouletteManager.Core.Interfaces
{
    public interface IRouletteRepository
    {

        Task<IEnumerable<Roulette>> GetRoulettes();
        Task<Roulette> GetRoulette(int id);
        Task<Roulette> PostRoulette(Roulette roulette);
        Task changeState(int id, int state);
    }
}
