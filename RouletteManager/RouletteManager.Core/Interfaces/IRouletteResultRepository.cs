﻿using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RouletteManager.Core.Interfaces
{
    public interface IRouletteResultRepository
    {
        Task<RouletteResult> addRouletteResult(RouletteResult rouletteResult);
        Task<RouletteResult> getOpenRouletteResult(int idRoulette);
    }
}
