﻿using AutoMapper;
using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using RouletteManager.Core.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouletteManager.Core.Services
{
    public class RouletteService: IRouletteService
    {

        private readonly IRouletteRepository _rouletteRepository;
        private readonly IMapper _mapper;

        public RouletteService(IRouletteRepository rouletteRepository, IMapper mapper)
        {
            _rouletteRepository = rouletteRepository;
            _mapper = mapper;
        }

        public async Task<RouletteDTO> GetRoulette(int id)
        {
            Roulette response= await _rouletteRepository.GetRoulette(id);
            RouletteDTO rouletteDTO = _mapper.Map<RouletteDTO>(response);
            return rouletteDTO;
        }

        public async Task<IEnumerable<RouletteDTO>> GetRoulettes()
        {
            var response= await _rouletteRepository.GetRoulettes();

            IEnumerable<RouletteDTO> roulettesDTO = _mapper.Map<IEnumerable<RouletteDTO>>(response);
            return roulettesDTO;
        }

        public async Task<RouletteDTO> PostRoulette(RouletteDTO rouletteDTO)
        {
            Roulette roulette = _mapper.Map<Roulette>(rouletteDTO);
            var result = await _rouletteRepository.PostRoulette(roulette);
            rouletteDTO = _mapper.Map<RouletteDTO>(result);

            return rouletteDTO;
        }
    }
}
