﻿using RouletteManager.Core.Interfaces;

namespace RouletteManager.Core.Services
{

    public class UsersService: IUsersService
    {
        private readonly IUsersRepository _UsersRepository;

        public UsersService(IUsersRepository UsersRepository)
        {
            _UsersRepository = UsersRepository;
        }

    }
}
