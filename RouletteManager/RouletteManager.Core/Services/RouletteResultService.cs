﻿using AutoMapper;
using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using RouletteManager.Core.Interfaces;
using System.Net;
using System.Threading.Tasks;

namespace RouletteManager.Core.Services
{
    public class RouletteResultService: IRouletteResultService
    {
        private readonly IRouletteResultRepository _rouletteResultRepository;
        private readonly IRouletteRepository _rouletteRepository;
        private readonly IMapper _mapper;

        public RouletteResultService(IRouletteResultRepository rouletteResultRepository, IRouletteRepository rouletteRepository, IMapper mapper)
        {
            _rouletteResultRepository = rouletteResultRepository;
            _rouletteRepository = rouletteRepository;
            _mapper = mapper;
        }

        public async Task<RouletteResultDTO> postRouletteResult(RouletteResultDTO rouletteResultDTO)
        {

            
            var roulette= await _rouletteRepository.GetRoulette(rouletteResultDTO.IdRoulette);
            if (roulette != null)
            {
                if (roulette.State == 0)
                {
                    
                    var rouletteResult = _mapper.Map<RouletteResult>(rouletteResultDTO);
                    var result= await _rouletteResultRepository.addRouletteResult(rouletteResult);
                    await _rouletteRepository.changeState(rouletteResultDTO.IdRoulette,1);
                    rouletteResultDTO = _mapper.Map<RouletteResultDTO>(result);
                    rouletteResultDTO.ResponseMesagge = "Ruleta abierta exitosamente";
                    rouletteResultDTO.ResponseCode = HttpStatusCode.OK.ToString();

                }
                else {
                    rouletteResultDTO.ResponseMesagge = "Imposible abrir ruleta, esta ya se encuentra abierta";
                    rouletteResultDTO.ResponseCode = HttpStatusCode.BadRequest.ToString();
                }
            }
            else {

                rouletteResultDTO.ResponseMesagge = "Ruleta no encontrada";
                rouletteResultDTO.ResponseCode = HttpStatusCode.BadRequest.ToString();
            }

            return rouletteResultDTO;
        }
    }
}
