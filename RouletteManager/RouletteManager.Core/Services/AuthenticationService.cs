﻿using RouletteManager.Core.Interfaces;

namespace RouletteManager.Core.Services
{
    public class AuthenticationService: IAuthenticationService
    {

        private readonly IAuthenticationRepository _authenticationRepository;

        public AuthenticationService(IAuthenticationRepository authenticationRepository) {
            _authenticationRepository = authenticationRepository;
        }

    }
}
