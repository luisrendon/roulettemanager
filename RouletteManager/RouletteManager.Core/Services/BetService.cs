﻿using AutoMapper;
using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using RouletteManager.Core.Interfaces;
using System.Net;
using System.Threading.Tasks;

namespace RouletteManager.Core.Services
{
    public class BetService: IBetService
    {
        private readonly IBetRepository _betRepository;
        private readonly IRouletteResultRepository _rouletteResultRepository;
        private readonly IRouletteRepository _rouletteRepository;
        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;
        

        public BetService(IBetRepository betRepository, IMapper mapper, IRouletteResultRepository rouletteResultRepository, IRouletteRepository rouletteRepository, IUsersRepository usersRepository)
        {
            _betRepository = betRepository;
            _rouletteResultRepository = rouletteResultRepository;
            _rouletteRepository = rouletteRepository;
            _usersRepository = usersRepository;
            _mapper = mapper;

        }

        public async Task<BetDTO> PostBet(BetDTO betDTO, int idUser)
        {
            var roulette = await _rouletteRepository.GetRoulette(betDTO.IdRoulette);
            if (roulette != null)
            {
                if (roulette.State == 1) {
                    RouletteResult rouletteResult = await _rouletteResultRepository.getOpenRouletteResult(betDTO.IdRoulette);
                    Users users = await _usersRepository.getUserById(idUser);
                    if (users != null)
                    {
                        if (users.Credit >= betDTO.BetValue)
                        {
                            betDTO.IdRouletteResult = rouletteResult.IdRouletteResult;
                            Bet bet= _mapper.Map<Bet>(betDTO);
                            bet.IdUser = idUser;
                            Bet response = await _betRepository.postBet(bet);
                            betDTO = _mapper.Map<BetDTO>(response);
                            
                            betDTO.ResponseMesagge = "Apuesta registrada con éxito";
                            betDTO.ResponseCode = HttpStatusCode.OK.ToString();
                        }
                        else {
                            betDTO.ResponseMesagge = "Usuario sin crédito";
                            betDTO.ResponseCode = HttpStatusCode.BadRequest.ToString();
                        }
                    }
                    else {
                        betDTO.ResponseMesagge = "Usuario no existe";
                        betDTO.ResponseCode = HttpStatusCode.BadRequest.ToString();
                    }
                }
                else {

                    betDTO.ResponseMesagge = "La ruleta está cerrada";
                    betDTO.ResponseCode = HttpStatusCode.BadRequest.ToString();
                }
            }
            else {
                betDTO.ResponseMesagge = "No existe la ruletta";
                betDTO.ResponseCode = HttpStatusCode.BadRequest.ToString();
            }

            return betDTO;
        }
    }
}
