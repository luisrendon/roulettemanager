﻿using System.Collections.Generic;

namespace RouletteManager.Core.Data
{
    public partial class Roulette
    {
        public Roulette()
        {
            RouletteResult = new HashSet<RouletteResult>();
        }

        public int IdRoulette { get; set; }
        public int State { get; set; }

        public virtual ICollection<RouletteResult> RouletteResult { get; set; }
    }
}
