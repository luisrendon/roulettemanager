﻿using System.Collections.Generic;

namespace RouletteManager.Core.Data
{
    public partial class RouletteResult
    {
        public RouletteResult()
        {
            Bet = new HashSet<Bet>();
        }

        public int IdRouletteResult { get; set; }
        public int IdRoulette { get; set; }
        public decimal Acumulated { get; set; }
        public decimal? ColorResult { get; set; }
        public decimal? NumerResult { get; set; }

        public virtual Roulette IdRouletteNavigation { get; set; }
        public virtual ICollection<Bet> Bet { get; set; }
    }
}
