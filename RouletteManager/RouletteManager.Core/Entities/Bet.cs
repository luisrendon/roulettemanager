﻿namespace RouletteManager.Core.Data
{
    public partial class Bet
    {
        public int IdBet { get; set; }
        public int IdRouletteResult { get; set; }
        public int IdUser { get; set; }
        public decimal BetNumber { get; set; }
        public decimal BetValue { get; set; }
        public decimal BetColor { get; set; }
        public int State { get; set; }

        public virtual RouletteResult IdRouletteResultNavigation { get; set; }
        public virtual Users IdUserNavigation { get; set; }
    }
}
