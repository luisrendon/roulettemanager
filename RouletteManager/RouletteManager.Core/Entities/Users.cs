﻿using System.Collections.Generic;

namespace RouletteManager.Core.Data
{
    public partial class Users
    {
        public Users()
        {
            Bet = new HashSet<Bet>();
        }

        public int IdUser { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public decimal Credit { get; set; }
        public int State { get; set; }

        public virtual ICollection<Bet> Bet { get; set; }
    }
}
