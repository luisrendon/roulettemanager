using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RouletteManager.Core.Interfaces;
using RouletteManager.Core.Services;
using RouletteManager.Infraestructure.Data;
using RouletteManager.Infraestructure.Filters;
using RouletteManager.Infraestructure.Repositories;
using System;
using System.Text;

namespace RouletteManager.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddControllers();
            services.AddDbContext<BD_ROULETTEContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("RouletteManager")));

            services.AddTransient<IRouletteRepository, RouletteRepository>();
            services.AddTransient<IBetRepository, BetRepository>();
            services.AddTransient<IUsersRepository, UsersRepository>();
            services.AddTransient<IRouletteResultRepository, RouletteResultRepository>();
            services.AddTransient<IAuthenticationRepository, AuthenticationRepository>();

            services.AddTransient<IRouletteService, RouletteService>();
            services.AddTransient<IBetService, BetService>();
            services.AddTransient<IUsersService, UsersService>();
            services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddTransient<IRouletteResultService, RouletteResultService>();

            services.AddAuthentication(options=> {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(options=> {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey=true,
                        ValidIssuer=Configuration["Authentication:Issuer"],
                        ValidAudience= Configuration["Authentication:Audience"],
                        IssuerSigningKey= new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Authentication:SecretKey"]))
                    };
                });

            services.AddSwaggerGen(doc =>
            {
                doc.SwaggerDoc("v1", new OpenApiInfo { 
                    Title= "RouletteManagerAPI",
                    Version="v1"
                });
            });
            services.AddMvc(options => {
                options.Filters.Add<ModelValidationFilter>();
            }).AddFluentValidation(options=> {
                options.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(options=> {
                options.SwaggerEndpoint("/swagger/v1/swagger.json","Roulette Manager API");
                options.RoutePrefix = string.Empty; 
            });

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
