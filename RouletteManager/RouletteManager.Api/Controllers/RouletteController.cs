﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using RouletteManager.Core.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using ActionResult = Microsoft.AspNetCore.Mvc.ActionResult;
namespace RouletteManager.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteController : ControllerBase
    {
        private readonly IRouletteService _rouletteService;
     
        public RouletteController(IRouletteService rouletteService) {
            _rouletteService = rouletteService;
            
        }

        [HttpGet]
        public async Task<ActionResult> GetRoulettes() {
            
            var roulettesDTO =await _rouletteService.GetRoulettes();
         
            return Ok(roulettesDTO);

        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetRoulette(int id)
        {
            var rouletteDTO = await _rouletteService.GetRoulette(id);
            
            return Ok(rouletteDTO);
        }

        [HttpPost]
        public async Task<ActionResult> PostRoulette(RouletteDTO rouletteDTO)
        {
          
           RouletteDTO respuesta = await _rouletteService.PostRoulette(rouletteDTO);

            return Ok(rouletteDTO);
        }

    }
}
