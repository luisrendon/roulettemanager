﻿using Microsoft.AspNetCore.Mvc;
using RouletteManager.Core.DTOs;
using RouletteManager.Core.Services;
using System.Net;
using System.Threading.Tasks;

namespace RouletteManager.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteResultController : ControllerBase
    {

        private readonly IRouletteResultService _rouletteResultService;
        
        public RouletteResultController(IRouletteResultService rouletteResultService)
        {
            _rouletteResultService = rouletteResultService;
          
        }


        [HttpPost]
        public async Task<ActionResult> postRouletteResult(RouletteResultDTO rouletteResultDTO)
        {
            RouletteResultDTO respuesta =await _rouletteResultService.postRouletteResult(rouletteResultDTO);

            if (respuesta.ResponseCode.Equals(HttpStatusCode.OK.ToString()))
            {
                return Ok(respuesta);
            }
            else {
                return BadRequest(respuesta);
            }
        }

    }
}
