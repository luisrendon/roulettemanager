﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RouletteManager.Core.DTOs;
using RouletteManager.Core.Services;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RouletteManager.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BetController : ControllerBase
    {

        private readonly IBetService _betService;

        public BetController(IBetService betService)
        {
            _betService = betService;

        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> PostBet(BetDTO betDTO)
        {
            int idUser = Int32.Parse(Request.Headers["IdUser"]);
            BetDTO response = await _betService.PostBet(betDTO, idUser);
            if (response.ResponseCode.Equals(HttpStatusCode.BadRequest.ToString()))
            {
                return BadRequest(betDTO);
            }
            else
            {
                return Ok(betDTO);
            }


        }

    }
}
