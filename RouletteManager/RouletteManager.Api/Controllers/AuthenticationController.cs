﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using RouletteManager.Core.DTOs;
using System;
using System.Text;

using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using RouletteManager.Infraestructure.Repositories;
using RouletteManager.Core.Interfaces;
using RouletteManager.Core.Data;

namespace RouletteManager.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly IAuthenticationRepository _authenticationRepository;

        public AuthenticationController(IMapper mapper, IConfiguration configuration, IAuthenticationRepository authenticationRepository)
        {
            _mapper = mapper;
            _configuration = configuration;
            _authenticationRepository = authenticationRepository;
        }

        [HttpPost]
        public IActionResult Authentication(UsersDTO usersDTO) {

            var users = _mapper.Map<Users>(usersDTO);
            bool auth= _authenticationRepository.Authentication(users);
            if (auth)
            {
                var token = generateToken(usersDTO);
                usersDTO.token = token;
            }
            else {
                return NotFound(usersDTO);
            }
            return Ok(usersDTO);
        }

        private String generateToken(UsersDTO usersDTO) {
            var _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Authentication:SecretKey"]));
            var signingCredentials = new SigningCredentials(_symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);
            var header = new JwtHeader(signingCredentials);
            var claims = new[] {
                new Claim(ClaimTypes.Name, usersDTO.Username)
            };
            var payload = new JwtPayload(
            _configuration["Authentication:Issuer"],
            _configuration["Authentication:Audience"],
            claims,
            DateTime.Now,
            DateTime.UtcNow.AddMinutes(300)
            );

            var token = new JwtSecurityToken(header, payload);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }


    }
}
