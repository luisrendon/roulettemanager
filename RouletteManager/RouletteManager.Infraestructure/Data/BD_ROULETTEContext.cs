﻿using Microsoft.EntityFrameworkCore;
using RouletteManager.Core.Data;
using RouletteManager.Infraestructure.Data.Configurations;

namespace RouletteManager.Infraestructure.Data
{
    public partial class BD_ROULETTEContext : DbContext
    {
        public BD_ROULETTEContext()
        {
        }

        public BD_ROULETTEContext(DbContextOptions<BD_ROULETTEContext> options)
            : base(options)
        {  
        }

        public virtual DbSet<Bet> Bet { get; set; }
        public virtual DbSet<Roulette> Roulette { get; set; }
        public virtual DbSet<RouletteResult> RouletteResult { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BetConfiguration());
            modelBuilder.ApplyConfiguration(new RouletteConfiguration());
            modelBuilder.ApplyConfiguration(new RouletteResultConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());

        }

    }
}
