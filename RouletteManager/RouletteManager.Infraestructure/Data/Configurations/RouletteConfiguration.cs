﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RouletteManager.Core.Data;

namespace RouletteManager.Infraestructure.Data.Configurations
{
    public class RouletteConfiguration : IEntityTypeConfiguration<Roulette>
    {
        public void Configure(EntityTypeBuilder<Roulette> builder)
        {
            builder.HasKey(e => e.IdRoulette);

            builder.ToTable("ROULETTE");

            builder.Property(e => e.IdRoulette).HasColumnName("ID_ROULETTE");

            builder.Property(e => e.State)
                .HasColumnName("STATE")
                .HasDefaultValueSql("((1))")
                .HasComment("Roulette state: 0=inactive; 1=active;");
        }
    }
}
