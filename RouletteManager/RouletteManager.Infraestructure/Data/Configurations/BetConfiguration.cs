﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RouletteManager.Core.Data;

namespace RouletteManager.Infraestructure.Data.Configurations
{
    class BetConfiguration : IEntityTypeConfiguration<Bet>
    {
        public void Configure(EntityTypeBuilder<Bet> builder)
        {
            builder.HasKey(e => e.IdBet);

            builder.ToTable("BET");

            builder.Property(e => e.IdBet).HasColumnName("ID_BET");

            builder.Property(e => e.BetColor)
                .HasColumnName("BET_COLOR")
                .HasColumnType("numeric(1, 0)");

            builder.Property(e => e.BetNumber)
                .HasColumnName("BET_NUMBER")
                .HasColumnType("numeric(2, 0)");

            builder.Property(e => e.BetValue)
                .HasColumnName("BET_VALUE")
                .HasColumnType("decimal(20, 0)");

            builder.Property(e => e.IdRouletteResult).HasColumnName("ID_ROULETTE_RESULT");

            builder.Property(e => e.IdUser).HasColumnName("ID_USER");

            builder.Property(e => e.State)
                .HasColumnName("STATE")
                .HasDefaultValueSql("((1))")
                .HasComment("Bet state: closed=0; open=1;");

            builder.HasOne(d => d.IdRouletteResultNavigation)
                .WithMany(p => p.Bet)
                .HasForeignKey(d => d.IdRouletteResult)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_BET_ROULETTE_RESULT");

            builder.HasOne(d => d.IdUserNavigation)
                .WithMany(p => p.Bet)
                .HasForeignKey(d => d.IdUser)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_BET_USERS");
        }
    }
}
