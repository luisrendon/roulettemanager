﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RouletteManager.Core.Data;

namespace RouletteManager.Infraestructure.Data.Configurations
{
    class RouletteResultConfiguration : IEntityTypeConfiguration<RouletteResult>
    {
        public void Configure(EntityTypeBuilder<RouletteResult> builder)
        {
            builder.HasKey(e => e.IdRouletteResult);

            builder.ToTable("ROULETTE_RESULT");

            builder.Property(e => e.IdRouletteResult).HasColumnName("ID_ROULETTE_RESULT");

            builder.Property(e => e.Acumulated)
                .HasColumnName("ACUMULATED")
                .HasColumnType("decimal(20, 2)");

            builder.Property(e => e.ColorResult)
                .HasColumnName("COLOR_RESULT")
                .HasColumnType("numeric(1, 0)")
                .HasComment("COLOR RESULT:  0=CLOSED, 1= OPEN");

            builder.Property(e => e.IdRoulette).HasColumnName("ID_ROULETTE");

            builder.Property(e => e.NumerResult)
                .HasColumnName("NUMER_RESULT")
                .HasColumnType("numeric(2, 0)");

            builder.HasOne(d => d.IdRouletteNavigation)
                .WithMany(p => p.RouletteResult)
                .HasForeignKey(d => d.IdRoulette)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ROULETTE_RESULT_ROULETTE");
        }
    }
}
