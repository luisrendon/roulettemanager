﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RouletteManager.Core.Data;

namespace RouletteManager.Infraestructure.Data.Configurations
{
    class UserConfiguration : IEntityTypeConfiguration<Users>
    {
        public void Configure(EntityTypeBuilder<Users> builder)
        {
            builder.HasKey(e => e.IdUser)
                     .HasName("PK_USER");

            builder.ToTable("USERS");

            builder.Property(e => e.IdUser).HasColumnName("ID_USER");

            builder.Property(e => e.Credit)
                .HasColumnName("CREDIT")
                .HasColumnType("decimal(20, 2)");

            builder.Property(e => e.Password)
                .IsRequired()
                .HasColumnName("PASSWORD")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.State)
                .HasColumnName("STATE")
                .HasDefaultValueSql("((1))")
                .HasComment("User state: 0=inactive; 1=active");

            builder.Property(e => e.Username)
                .IsRequired()
                .HasColumnName("USERNAME")
                .HasMaxLength(50)
                .IsUnicode(false);
        }
    }
}
