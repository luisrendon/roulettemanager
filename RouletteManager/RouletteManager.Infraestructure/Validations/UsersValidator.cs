﻿using FluentValidation;
using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace RouletteManager.Infraestructure.Validations
{
    public class UsersValidator:AbstractValidator<UsersDTO>
    {
        public UsersValidator() {
            RuleFor(Users => Users.Password).NotNull();
            RuleFor(Users => Users.Username).NotNull();
        }

    }
}
