﻿using FluentValidation;
using RouletteManager.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace RouletteManager.Infraestructure.Validations
{
    public class BetValidator:AbstractValidator<BetDTO>
    {
        public BetValidator() {
            RuleFor(bet => bet.BetColor).NotNull();
            RuleFor(bet => bet.BetNumber).NotNull();
            RuleFor(bet => bet.BetValue).NotNull();
            RuleFor(bet => bet.IdRoulette).NotNull();

        }

    }
}
