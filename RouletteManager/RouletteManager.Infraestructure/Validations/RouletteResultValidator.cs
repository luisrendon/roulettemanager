﻿using FluentValidation;
using RouletteManager.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace RouletteManager.Infraestructure.Validations
{
    public class RouletteResultValidator: AbstractValidator<RouletteResultDTO>
    {
        public RouletteResultValidator() {
            RuleFor(RouletteResult => RouletteResult.IdRoulette).NotNull();
        }

    }
}
