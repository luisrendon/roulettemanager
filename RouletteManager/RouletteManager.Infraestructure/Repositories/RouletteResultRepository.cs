﻿using Microsoft.EntityFrameworkCore;
using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using RouletteManager.Core.Interfaces;
using RouletteManager.Infraestructure.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteManager.Infraestructure.Repositories
{
    public class RouletteResultRepository : IRouletteResultRepository
    {
        private readonly BD_ROULETTEContext _ROULETTEContext;
        public RouletteResultRepository(BD_ROULETTEContext ROULETTEContext)
        {
            _ROULETTEContext = ROULETTEContext;
        }

        public async Task<RouletteResult> addRouletteResult(RouletteResult rouletteResult)
        {
            _ROULETTEContext.RouletteResult.Add(rouletteResult);
            await _ROULETTEContext.SaveChangesAsync();
            return rouletteResult;
        }

        public async Task<RouletteResult> getOpenRouletteResult(int idRoulette)
        {
            RouletteResult rouletteResult =
                await _ROULETTEContext.RouletteResult.Where(rs => rs.IdRoulette == idRoulette)
                .OrderByDescending(r => r.IdRouletteResult).FirstAsync();

            return rouletteResult;
        }
    }
}
