﻿using Microsoft.EntityFrameworkCore;
using RouletteManager.Core.Data;
using RouletteManager.Core.Interfaces;
using RouletteManager.Infraestructure.Data;
using System.Threading.Tasks;

namespace RouletteManager.Infraestructure.Repositories
{
    public class BetRepository: IBetRepository
    {

        private readonly BD_ROULETTEContext _ROULETTEContext;
        public BetRepository(BD_ROULETTEContext ROULETTEContext)
        {
            _ROULETTEContext = ROULETTEContext;
        }

        public async Task<Bet> postBet(Bet bet)
        {
            _ROULETTEContext.Bet.Add(bet);
            Users user= await _ROULETTEContext.Users.FirstOrDefaultAsync(u=> u.IdUser==bet.IdUser);
            user.Credit = user.Credit - bet.BetValue;
            await _ROULETTEContext.SaveChangesAsync();
            return bet;
        }
    }
}
