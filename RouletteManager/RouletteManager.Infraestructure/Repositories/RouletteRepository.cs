﻿using Microsoft.EntityFrameworkCore;
using RouletteManager.Core.Data;
using RouletteManager.Core.Interfaces;
using RouletteManager.Infraestructure.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouletteManager.Infraestructure.Repositories
{


    public class RouletteRepository : IRouletteRepository
    {
        private readonly BD_ROULETTEContext _ROULETTEContext;
        public RouletteRepository(BD_ROULETTEContext ROULETTEContext)
        {
            _ROULETTEContext = ROULETTEContext;
        }

        public async Task<IEnumerable<Roulette>> GetRoulettes()
        {
            var roulette = await _ROULETTEContext.Roulette.ToListAsync();
            return roulette;
        }


        public async Task<Roulette> GetRoulette(int id)
        {
            var roulette = await _ROULETTEContext.Roulette.FirstOrDefaultAsync(
                x => x.IdRoulette == id);
            return roulette;
        }

        public async Task<Roulette>  PostRoulette(Roulette roulette)
        {
            _ROULETTEContext.Roulette.Add(roulette);
             await _ROULETTEContext.SaveChangesAsync();
            return roulette;
        }

        public async Task changeState(int id, int state)
        {
            var roulette = await _ROULETTEContext.Roulette.FirstOrDefaultAsync(x => x.IdRoulette == id);
            roulette.State = state;
            await _ROULETTEContext.SaveChangesAsync();
        }
    }
}