﻿using Microsoft.EntityFrameworkCore;
using RouletteManager.Core.Data;
using RouletteManager.Core.Interfaces;
using RouletteManager.Infraestructure.Data;
using System.Threading.Tasks;

namespace RouletteManager.Infraestructure.Repositories
{
    public class UsersRepository: IUsersRepository
    {
        private readonly BD_ROULETTEContext _ROULETTEContext;
        public UsersRepository(BD_ROULETTEContext ROULETTEContext)
        {
            _ROULETTEContext = ROULETTEContext;
        }

        public async Task<Users> getUserById(int idUser)
        {
            var user = await _ROULETTEContext.Users.FirstOrDefaultAsync(u => u.IdUser == idUser);
            return user;
        }
    }
}
