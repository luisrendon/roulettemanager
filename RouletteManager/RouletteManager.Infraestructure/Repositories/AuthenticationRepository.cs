﻿using Microsoft.EntityFrameworkCore;
using RouletteManager.Core.Data;
using RouletteManager.Core.Interfaces;
using RouletteManager.Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouletteManager.Infraestructure.Repositories
{
    public class AuthenticationRepository : IAuthenticationRepository
    {

        private readonly BD_ROULETTEContext _ROULETTEContext;
        public AuthenticationRepository(BD_ROULETTEContext ROULETTEContext)
        {
            _ROULETTEContext = ROULETTEContext;
        }

        public bool Authentication(Users users)
        {
            if (_ROULETTEContext.Users.Any
                (u => u.Username==users.Username && u.Password==users.Password)) {
                return true;
            }
            return false;
        }
    }
}
