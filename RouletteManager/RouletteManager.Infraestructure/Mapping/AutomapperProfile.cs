﻿using AutoMapper;
using RouletteManager.Core.Data;
using RouletteManager.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace RouletteManager.Infraestructure.Mapping
{
    public class AutomapperProfile: Profile
    {
        public AutomapperProfile() {
            CreateMap<Bet, BetDTO>();
            CreateMap<BetDTO, Bet>();
            CreateMap<Roulette, RouletteDTO>();
            CreateMap<RouletteDTO, Roulette>();
            CreateMap<Users, UsersDTO>();
            CreateMap<UsersDTO, Users>();
            CreateMap<RouletteResult, RouletteResultDTO>();
            CreateMap<RouletteResultDTO, RouletteResult>();
        }

    }
}
